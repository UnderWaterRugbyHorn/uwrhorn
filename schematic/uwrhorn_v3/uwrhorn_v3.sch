EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "Breakout Board"
Date "2020-05-16"
Rev "v3.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2950 2900 2950 3000
Wire Wire Line
	3750 2900 3750 3000
Wire Wire Line
	3750 2450 3500 2450
Wire Wire Line
	3750 2600 3750 2450
Wire Wire Line
	2950 2600 2950 2450
Wire Wire Line
	3200 2450 2950 2450
$Comp
L Device:R R2
U 1 1 5EA5F5E2
P 2950 2750
F 0 "R2" H 3020 2796 50  0000 L CNN
F 1 "1k" H 3020 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2880 2750 50  0001 C CNN
F 3 "https://cdn-reichelt.de/documents/datenblatt/A300/RND_SMD_RESISTORS_ENG_TDS.pdf" H 2950 2750 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-widerstand-0805-1-0-kohm-125-mw-1-rnd-0805-1-1-0k-p183228.html?&nbc=1" H 2950 2750 50  0001 C CNN "Buy_Link"
	1    2950 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:D D2
U 1 1 5EA6559E
P 2400 2750
F 0 "D2" V 2446 2671 50  0000 R CNN
F 1 "1N4148" V 2355 2671 50  0000 R CNN
F 2 "custom_footprints:D_SOD-123_0805_combo" H 2400 2750 50  0001 C CNN
F 3 "https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=A200%2FRND_1N4148W_DB-EN.pdf" H 2400 2750 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/schalt-diode-75-v-150-ma-sod-123-rnd-1n4148w-p223369.html?&nbc=1" H 2400 2750 50  0001 C CNN "Buy_Link"
	1    2400 2750
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5EA6E851
P 3750 3000
F 0 "#PWR010" H 3750 2750 50  0001 C CNN
F 1 "GND" H 3755 2827 50  0000 C CNN
F 2 "" H 3750 3000 50  0001 C CNN
F 3 "" H 3750 3000 50  0001 C CNN
	1    3750 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EA6365D
P 3750 2750
F 0 "C1" H 3865 2796 50  0000 L CNN
F 1 "100n" H 3865 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3788 2600 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=B300%2FX7R-G0805%23YAG.pdf" H 3750 2750 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-vielschicht-keramikkondensator-100n-10-x7r-g0805-100n-p31879.html?&nbc=1" H 3750 2750 50  0001 C CNN "Buy_Link"
	1    3750 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5EA62928
P 2950 3000
F 0 "#PWR07" H 2950 2750 50  0001 C CNN
F 1 "GND" H 2955 2827 50  0000 C CNN
F 2 "" H 2950 3000 50  0001 C CNN
F 3 "" H 2950 3000 50  0001 C CNN
	1    2950 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5EA60A2D
P 3350 2450
F 0 "R1" V 3143 2450 50  0000 C CNN
F 1 "10k" V 3234 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3280 2450 50  0001 C CNN
F 3 "https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=A300%2FRND_SMD_RESISTORS_ENG_TDS.pdf" H 3350 2450 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-widerstand-0805-10-kohm-125-mw-1-rnd-0805-1-10k-p183251.html?&nbc=1" H 3350 2450 50  0001 C CNN "Buy_Link"
	1    3350 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 3950 2950 4050
Wire Wire Line
	3750 3950 3750 4050
Wire Wire Line
	3750 3500 3500 3500
Wire Wire Line
	3750 3650 3750 3500
Wire Wire Line
	2950 3650 2950 3500
Wire Wire Line
	3200 3500 2950 3500
$Comp
L Device:R R4
U 1 1 5EAAD977
P 2950 3800
F 0 "R4" H 3020 3846 50  0000 L CNN
F 1 "1k" H 3020 3755 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2880 3800 50  0001 C CNN
F 3 "https://cdn-reichelt.de/documents/datenblatt/A300/RND_SMD_RESISTORS_ENG_TDS.pdf" H 2950 3800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-widerstand-0805-1-0-kohm-125-mw-1-rnd-0805-1-1-0k-p183228.html?&nbc=1" H 2950 3800 50  0001 C CNN "Buy_Link"
	1    2950 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:D D3
U 1 1 5EAAD981
P 2400 3800
F 0 "D3" V 2446 3721 50  0000 R CNN
F 1 "1N4148" V 2355 3721 50  0000 R CNN
F 2 "custom_footprints:D_SOD-123_0805_combo" H 2400 3800 50  0001 C CNN
F 3 "https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=A200%2FRND_1N4148W_DB-EN.pdf" H 2400 3800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/schalt-diode-75-v-150-ma-sod-123-rnd-1n4148w-p223369.html?&nbc=1" H 2400 3800 50  0001 C CNN "Buy_Link"
	1    2400 3800
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5EAAD98B
P 3750 4050
F 0 "#PWR011" H 3750 3800 50  0001 C CNN
F 1 "GND" H 3755 3877 50  0000 C CNN
F 2 "" H 3750 4050 50  0001 C CNN
F 3 "" H 3750 4050 50  0001 C CNN
	1    3750 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5EAAD995
P 3750 3800
F 0 "C2" H 3865 3846 50  0000 L CNN
F 1 "100n" H 3865 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3788 3650 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=B300%2FX7R-G0805%23YAG.pdf" H 3750 3800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-vielschicht-keramikkondensator-100n-10-x7r-g0805-100n-p31879.html?&nbc=1" H 3750 3800 50  0001 C CNN "Buy_Link"
	1    3750 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5EAAD99F
P 2950 4050
F 0 "#PWR08" H 2950 3800 50  0001 C CNN
F 1 "GND" H 2955 3877 50  0000 C CNN
F 2 "" H 2950 4050 50  0001 C CNN
F 3 "" H 2950 4050 50  0001 C CNN
	1    2950 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5EAAD9A9
P 3350 3500
F 0 "R3" V 3143 3500 50  0000 C CNN
F 1 "10k" V 3234 3500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3280 3500 50  0001 C CNN
F 3 "https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=A300%2FRND_SMD_RESISTORS_ENG_TDS.pdf" H 3350 3500 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-widerstand-0805-10-kohm-125-mw-1-rnd-0805-1-10k-p183251.html?&nbc=1" H 3350 3500 50  0001 C CNN "Buy_Link"
	1    3350 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5EABF0ED
P 3350 4500
F 0 "R5" V 3143 4500 50  0000 C CNN
F 1 "10k" V 3234 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3280 4500 50  0001 C CNN
F 3 "https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=A300%2FRND_SMD_RESISTORS_ENG_TDS.pdf" H 3350 4500 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-widerstand-0805-10-kohm-125-mw-1-rnd-0805-1-10k-p183251.html?&nbc=1" H 3350 4500 50  0001 C CNN "Buy_Link"
	1    3350 4500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5EABF0F3
P 2950 5050
F 0 "#PWR09" H 2950 4800 50  0001 C CNN
F 1 "GND" H 2955 4877 50  0000 C CNN
F 2 "" H 2950 5050 50  0001 C CNN
F 3 "" H 2950 5050 50  0001 C CNN
	1    2950 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5EABF0F9
P 3750 4800
F 0 "C3" H 3865 4846 50  0000 L CNN
F 1 "100n" H 3865 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3788 4650 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=B300%2FX7R-G0805%23YAG.pdf" H 3750 4800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-vielschicht-keramikkondensator-100n-10-x7r-g0805-100n-p31879.html?&nbc=1" H 3750 4800 50  0001 C CNN "Buy_Link"
	1    3750 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5EABF0FF
P 3750 5050
F 0 "#PWR012" H 3750 4800 50  0001 C CNN
F 1 "GND" H 3755 4877 50  0000 C CNN
F 2 "" H 3750 5050 50  0001 C CNN
F 3 "" H 3750 5050 50  0001 C CNN
	1    3750 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:D D4
U 1 1 5EABF105
P 2400 4800
F 0 "D4" V 2446 4721 50  0000 R CNN
F 1 "1N4148" V 2355 4721 50  0000 R CNN
F 2 "custom_footprints:D_SOD-123_0805_combo" H 2400 4800 50  0001 C CNN
F 3 "https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=A200%2FRND_1N4148W_DB-EN.pdf" H 2400 4800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/schalt-diode-75-v-150-ma-sod-123-rnd-1n4148w-p223369.html?&nbc=1" H 2400 4800 50  0001 C CNN "Buy_Link"
	1    2400 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R6
U 1 1 5EABF10B
P 2950 4800
F 0 "R6" H 3020 4846 50  0000 L CNN
F 1 "1k" H 3020 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2880 4800 50  0001 C CNN
F 3 "https://cdn-reichelt.de/documents/datenblatt/A300/RND_SMD_RESISTORS_ENG_TDS.pdf" H 2950 4800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-widerstand-0805-1-0-kohm-125-mw-1-rnd-0805-1-1-0k-p183228.html?&nbc=1" H 2950 4800 50  0001 C CNN "Buy_Link"
	1    2950 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 4500 2950 4500
Wire Wire Line
	2950 4650 2950 4500
Connection ~ 2950 4500
Wire Wire Line
	2950 4500 2400 4500
Wire Wire Line
	3750 4650 3750 4500
Wire Wire Line
	3750 4500 3500 4500
Wire Wire Line
	3750 4950 3750 5050
Wire Wire Line
	2950 4950 2950 5050
Wire Wire Line
	2400 4500 2400 4650
$Comp
L Connector:Raspberry_Pi_2_3 RPI1
U 1 1 5EB06B24
P 6050 3150
F 0 "RPI1" H 6050 4631 50  0000 C CNN
F 1 "Raspberry_Pi_2_3" H 6050 1650 50  0000 C CNN
F 2 "custom_footprints:Raspberry4" H 6050 3150 50  0001 C CNN
F 3 "https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_3bplus_1p0_reduced.pdf" H 6050 3150 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/raspberry-pi-4-b-4x-1-5-ghz-2-gb-ram-wlan-bt-rasp-pi-4-b-2gb-p259919.html?&nbc=1" H 6050 3150 50  0001 C CNN "Buy_Link"
	1    6050 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5EB0C9C0
P 5650 4650
F 0 "#PWR020" H 5650 4400 50  0001 C CNN
F 1 "GND" H 5655 4477 50  0000 C CNN
F 2 "" H 5650 4650 50  0001 C CNN
F 3 "" H 5650 4650 50  0001 C CNN
	1    5650 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 4450 5650 4550
Wire Wire Line
	6350 4450 6350 4550
Wire Wire Line
	6350 4550 6250 4550
Connection ~ 5650 4550
Wire Wire Line
	5650 4550 5650 4650
Wire Wire Line
	6250 4450 6250 4550
Connection ~ 6250 4550
Wire Wire Line
	6250 4550 6150 4550
Wire Wire Line
	6150 4450 6150 4550
Connection ~ 6150 4550
Wire Wire Line
	6150 4550 6050 4550
Wire Wire Line
	6050 4450 6050 4550
Connection ~ 6050 4550
Wire Wire Line
	6050 4550 5950 4550
Wire Wire Line
	5950 4450 5950 4550
Connection ~ 5950 4550
Wire Wire Line
	5950 4550 5850 4550
Wire Wire Line
	5850 4450 5850 4550
Connection ~ 5850 4550
$Comp
L power:+3.3V #PWR024
U 1 1 5EB15CB7
P 6250 1550
F 0 "#PWR024" H 6250 1400 50  0001 C CNN
F 1 "+3.3V" H 6265 1723 50  0000 C CNN
F 2 "" H 6250 1550 50  0001 C CNN
F 3 "" H 6250 1550 50  0001 C CNN
	1    6250 1550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR022
U 1 1 5EB18A92
P 5950 1550
F 0 "#PWR022" H 5950 1400 50  0001 C CNN
F 1 "+5V" H 5965 1723 50  0000 C CNN
F 2 "" H 5950 1550 50  0001 C CNN
F 3 "" H 5950 1550 50  0001 C CNN
	1    5950 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1550 5950 1650
Wire Wire Line
	5950 1650 5850 1650
Wire Wire Line
	5850 1650 5850 1850
Connection ~ 5950 1650
Wire Wire Line
	5950 1650 5950 1850
Text Label 4850 2650 0    50   ~ 0
GPIO17
Wire Wire Line
	5250 2650 4850 2650
Wire Wire Line
	6850 2850 7200 2850
Text Label 7200 2850 2    50   ~ 0
GPIO4
Text Label 4850 3350 0    50   ~ 0
GPIO22
Text Label 4850 3850 0    50   ~ 0
GPIO27
Wire Wire Line
	4850 3350 5250 3350
Wire Wire Line
	4850 3850 5250 3850
Wire Wire Line
	3750 2450 4100 2450
Connection ~ 3750 2450
Wire Wire Line
	3750 3500 4100 3500
Connection ~ 3750 3500
Wire Wire Line
	3750 4500 4100 4500
Connection ~ 3750 4500
Text Label 4100 2450 2    50   ~ 0
GPIO17
Text Label 4100 3500 2    50   ~ 0
GPIO27
Text Label 4100 4500 2    50   ~ 0
GPIO22
Text Label 4100 2050 2    50   ~ 0
GPIO4
Text Label 7200 2550 2    50   ~ 0
SDA
Text Label 7200 2650 2    50   ~ 0
SCL
Wire Wire Line
	6850 2550 7200 2550
Wire Wire Line
	6850 2650 7200 2650
NoConn ~ 5250 2250
NoConn ~ 5250 2350
NoConn ~ 5250 2550
NoConn ~ 5250 2750
NoConn ~ 5250 2950
NoConn ~ 5250 3050
NoConn ~ 5250 3150
NoConn ~ 5250 3450
NoConn ~ 5250 3550
NoConn ~ 5250 3650
NoConn ~ 5250 3750
NoConn ~ 6850 2350
NoConn ~ 6850 2250
NoConn ~ 6850 2950
NoConn ~ 6850 3050
NoConn ~ 6850 3250
NoConn ~ 6850 3350
NoConn ~ 6850 3450
NoConn ~ 6850 3550
NoConn ~ 6850 3650
NoConn ~ 6850 3850
NoConn ~ 6850 3950
$Comp
L Device:R R7
U 1 1 5EB67950
P 2050 5700
F 0 "R7" H 1980 5654 50  0000 R CNN
F 1 "90" H 1980 5745 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1980 5700 50  0001 C CNN
F 3 "https://cdn-reichelt.de/bilder/elements/sonstige/64x64/pdf_64x64.png" H 2050 5700 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/smd-widerstand-0805-91-ohm-125-mw-1-rnd-0805-1-91-p183171.html?&nbc=1" H 2050 5700 50  0001 C CNN "Buy_Link"
	1    2050 5700
	-1   0    0    1   
$EndComp
$Comp
L Isolator:4N25 U1
U 1 1 5EB6A38A
P 2600 6400
F 0 "U1" H 2600 6725 50  0000 C CNN
F 1 "4N25" H 2600 6634 50  0000 C CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 2400 6200 50  0001 L CIN
F 3 "https://www.vishay.com/docs/83725/4n25.pdf" H 2600 6400 50  0001 L CNN
F 4 "https://secure.reichelt.com/de/de/optokoppler-4n-25-p2509.html?&trstct=pos_0&nbc=1" H 2600 6400 50  0001 C CNN "Buy_Link"
	1    2600 6400
	1    0    0    -1  
$EndComp
NoConn ~ 2900 6300
$Comp
L Transistor_BJT:BC547 Q1
U 1 1 5EB74191
P 3250 6800
F 0 "Q1" H 3441 6846 50  0000 L CNN
F 1 "BC547" H 3441 6755 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3450 6725 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 3250 6800 50  0001 L CNN
F 4 "https://secure.reichelt.com/de/de/bipolartransistor-npn-45v-0-1a-0-5w-to-92-bc-547b-p5006.html?&trstct=pos_1&nbc=1" H 3250 6800 50  0001 C CNN "Buy_Link"
	1    3250 6800
	1    0    0    -1  
$EndComp
$Comp
L custom_symbols:FRA2C RL1
U 1 1 5EB79DFD
P 4200 6000
F 0 "RL1" H 4630 6046 50  0000 L CNN
F 1 "FRA2C" H 4630 5955 50  0000 L CNN
F 2 "custom_footprints:FRA2C" H 4200 6000 50  0001 C CNN
F 3 "https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=C300%2FFRA2.pdf" H 4200 6000 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/hochstromrelais-fra2c-s2-12v-1-wechsler-30a-fra2c-s2-12v-p101959.html?&nbc=1" H 4200 6000 50  0001 C CNN "Buy_Link"
	1    4200 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 6400 4000 6300
Wire Wire Line
	3350 6400 4000 6400
Connection ~ 3350 6400
NoConn ~ 4300 5700
$Comp
L power:+12V #PWR018
U 1 1 5EB95459
P 4500 5500
F 0 "#PWR018" H 4500 5350 50  0001 C CNN
F 1 "+12V" H 4515 5673 50  0000 C CNN
F 2 "" H 4500 5500 50  0001 C CNN
F 3 "" H 4500 5500 50  0001 C CNN
	1    4500 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5700 4500 5550
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5EB9D8EC
P 4600 6800
F 0 "J2" H 4680 6792 50  0000 L CNN
F 1 "Horn 1" H 4680 6701 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 4600 6800 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=C100%2FRND_800774_ENG_TDS.pdf" H 4600 6800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/stiftleiste-2-pol-rm-5-08-mm-90-rnd-205-00199-p170494.html?&nbc=1&trstct=lsbght_sldr::170410" H 4600 6800 50  0001 C CNN "Buy_Link"
	1    4600 6800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5EB9E29F
P 5400 6800
F 0 "J3" H 5480 6792 50  0000 L CNN
F 1 "Horn 2" H 5480 6701 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 5400 6800 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=C100%2FRND_800774_ENG_TDS.pdf" H 5400 6800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/stiftleiste-2-pol-rm-5-08-mm-90-rnd-205-00199-p170494.html?&nbc=1&trstct=lsbght_sldr::170410" H 5400 6800 50  0001 C CNN "Buy_Link"
	1    5400 6800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5EB9E7EB
P 6050 6800
F 0 "J4" H 6130 6792 50  0000 L CNN
F 1 "Horn 3" H 6130 6701 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 6050 6800 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=C100%2FRND_800774_ENG_TDS.pdf" H 6050 6800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/stiftleiste-2-pol-rm-5-08-mm-90-rnd-205-00199-p170494.html?&nbc=1&trstct=lsbght_sldr::170410" H 6050 6800 50  0001 C CNN "Buy_Link"
	1    6050 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 6300 4400 6550
Wire Wire Line
	4400 6900 4400 7150
Connection ~ 4400 6550
Wire Wire Line
	4400 6550 4400 6800
Wire Wire Line
	5850 6550 5850 6800
Connection ~ 5200 6550
Wire Wire Line
	5200 7150 5200 6900
Wire Wire Line
	5850 7150 5850 6900
Wire Wire Line
	5200 6550 5850 6550
Wire Wire Line
	4400 6550 5200 6550
Wire Wire Line
	5200 6550 5200 6800
Wire Wire Line
	4400 7150 5200 7150
Wire Wire Line
	5850 7150 5200 7150
Connection ~ 5200 7150
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5EBC0C36
P 2800 1200
F 0 "#FLG02" H 2800 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 2800 1373 50  0000 C CNN
F 2 "" H 2800 1200 50  0001 C CNN
F 3 "~" H 2800 1200 50  0001 C CNN
	1    2800 1200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG04
U 1 1 5EBC6DD9
P 3700 1200
F 0 "#FLG04" H 3700 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 3700 1373 50  0000 C CNN
F 2 "" H 3700 1200 50  0001 C CNN
F 3 "~" H 3700 1200 50  0001 C CNN
	1    3700 1200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5EBC7170
P 2350 1200
F 0 "#FLG01" H 2350 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 2350 1373 50  0000 C CNN
F 2 "" H 2350 1200 50  0001 C CNN
F 3 "~" H 2350 1200 50  0001 C CNN
	1    2350 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5EBC7E08
P 3700 1200
F 0 "#PWR025" H 3700 950 50  0001 C CNN
F 1 "GND" H 3705 1027 50  0000 C CNN
F 2 "" H 3700 1200 50  0001 C CNN
F 3 "" H 3700 1200 50  0001 C CNN
	1    3700 1200
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5EBC861F
P 3250 1200
F 0 "#FLG03" H 3250 1275 50  0001 C CNN
F 1 "PWR_FLAG" H 3250 1373 50  0000 C CNN
F 2 "" H 3250 1200 50  0001 C CNN
F 3 "~" H 3250 1200 50  0001 C CNN
	1    3250 1200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR023
U 1 1 5EBC9270
P 3250 1200
F 0 "#PWR023" H 3250 1050 50  0001 C CNN
F 1 "+3.3V" H 3265 1373 50  0000 C CNN
F 2 "" H 3250 1200 50  0001 C CNN
F 3 "" H 3250 1200 50  0001 C CNN
	1    3250 1200
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR021
U 1 1 5EBCA02A
P 2800 1200
F 0 "#PWR021" H 2800 1050 50  0001 C CNN
F 1 "+5V" H 2815 1373 50  0000 C CNN
F 2 "" H 2800 1200 50  0001 C CNN
F 3 "" H 2800 1200 50  0001 C CNN
	1    2800 1200
	-1   0    0    1   
$EndComp
$Comp
L power:+12V #PWR019
U 1 1 5EBCAA19
P 2350 1200
F 0 "#PWR019" H 2350 1050 50  0001 C CNN
F 1 "+12V" H 2365 1373 50  0000 C CNN
F 2 "" H 2350 1200 50  0001 C CNN
F 3 "" H 2350 1200 50  0001 C CNN
	1    2350 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 5550 4000 5550
Wire Wire Line
	4000 5550 4000 5700
Connection ~ 4500 5550
Wire Wire Line
	4500 5500 4500 5550
$Comp
L custom_symbols:Adafruit_PCF8523_RTC RTC1
U 1 1 5EBF2821
P 4100 9100
F 0 "RTC1" H 4100 9900 66  0000 L CNN
F 1 "Adafruit_PCF8523_RTC" H 4100 9800 66  0000 L CNN
F 2 "custom_footprints:Adafruit_PCF8523" H 4750 9350 66  0001 C CNN
F 3 "https://cdn-learn.adafruit.com/downloads/pdf/adafruit-pcf8523-real-time-clock.pdf?timestamp=1589559881" H 4750 9350 66  0001 C CNN
F 4 "https://eckstein-shop.de/Adafruit-PCF8523-Real-Time-Clock-RTC-Assembled-Breakout-Board" H 4100 9100 50  0001 C CNN "Buy_Link"
	1    4100 9100
	1    0    0    -1  
$EndComp
NoConn ~ 3900 9150
Wire Wire Line
	3700 8550 3900 8550
Wire Wire Line
	3700 8700 3900 8700
Wire Wire Line
	3900 8850 3600 8850
Wire Wire Line
	3900 9000 3600 9000
Text Label 3600 8850 0    50   ~ 0
SDA
Text Label 3600 9000 0    50   ~ 0
SCL
Text Notes 4150 8100 0    50   ~ 0
Real Time Clock
$Comp
L power:GND #PWR013
U 1 1 5EC4AD9E
P 3350 7050
F 0 "#PWR013" H 3350 6800 50  0001 C CNN
F 1 "GND" H 3355 6877 50  0000 C CNN
F 2 "" H 3350 7050 50  0001 C CNN
F 3 "" H 3350 7050 50  0001 C CNN
	1    3350 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR017
U 1 1 5EC4B01E
P 4400 7200
F 0 "#PWR017" H 4400 6950 50  0001 C CNN
F 1 "GND" H 4405 7027 50  0000 C CNN
F 2 "" H 4400 7200 50  0001 C CNN
F 3 "" H 4400 7200 50  0001 C CNN
	1    4400 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 7150 4400 7200
Connection ~ 4400 7150
Wire Wire Line
	3350 7000 3350 7050
$Comp
L custom_symbols:USB_A_2Port USB1
U 1 1 5EC96DE0
P 6450 8750
F 0 "USB1" H 6400 9200 50  0000 C CNN
F 1 "USB_A_2Port" H 6400 9100 50  0000 C CNN
F 2 "Connector_USB:USB_A_Wuerth_61400826021_Horizontal_Stacked" H 6600 8800 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=C120%2FUSBAW-2.pdf" H 6600 8800 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/usb-einbaubuchse-a-gew-2-port-printmontage-usb-aw-2-p52004.html?&nbc=1" H 6450 8750 50  0001 C CNN "Buy_Link"
	1    6450 8750
	1    0    0    -1  
$EndComp
NoConn ~ 6750 8650
NoConn ~ 6750 8750
NoConn ~ 6750 9100
NoConn ~ 6750 9200
NoConn ~ 6300 9500
$Comp
L power:GND #PWR027
U 1 1 5ECA81A1
P 6900 8850
F 0 "#PWR027" H 6900 8600 50  0001 C CNN
F 1 "GND" V 6900 8700 50  0000 R CNN
F 2 "" H 6900 8850 50  0001 C CNN
F 3 "" H 6900 8850 50  0001 C CNN
	1    6900 8850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR029
U 1 1 5ECA8FD3
P 6900 9300
F 0 "#PWR029" H 6900 9050 50  0001 C CNN
F 1 "GND" V 6900 9150 50  0000 R CNN
F 2 "" H 6900 9300 50  0001 C CNN
F 3 "" H 6900 9300 50  0001 C CNN
	1    6900 9300
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR026
U 1 1 5ECA988C
P 6900 8550
F 0 "#PWR026" H 6900 8400 50  0001 C CNN
F 1 "+5V" V 6900 8700 50  0000 L CNN
F 2 "" H 6900 8550 50  0001 C CNN
F 3 "" H 6900 8550 50  0001 C CNN
	1    6900 8550
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR028
U 1 1 5ECAA279
P 6900 9000
F 0 "#PWR028" H 6900 8850 50  0001 C CNN
F 1 "+5V" V 6900 9150 50  0000 L CNN
F 2 "" H 6900 9000 50  0001 C CNN
F 3 "" H 6900 9000 50  0001 C CNN
	1    6900 9000
	0    1    1    0   
$EndComp
Wire Wire Line
	6750 8550 6900 8550
Wire Wire Line
	6900 8850 6750 8850
Wire Wire Line
	6750 9000 6900 9000
Wire Wire Line
	6900 9300 6750 9300
Text Notes 5600 1250 0    50   ~ 0
Raspberry Pi GPIO Pins
Text Notes 2700 900  0    50   ~ 0
Used Power Levels
Text Notes 6100 8200 0    50   ~ 0
USB for Powersupply of\nDisplay and Router
$Comp
L power:+12V #PWR030
U 1 1 5ED048B8
P 1650 10150
F 0 "#PWR030" H 1650 10000 50  0001 C CNN
F 1 "+12V" H 1650 10300 50  0000 C CNN
F 2 "" H 1650 10150 50  0001 C CNN
F 3 "" H 1650 10150 50  0001 C CNN
	1    1650 10150
	0    -1   -1   0   
$EndComp
Text Notes 1450 9950 0    50   ~ 0
Connectors from 12V\nDCDC Converter
Wire Wire Line
	1500 8900 1450 8900
Wire Wire Line
	1500 8700 1450 8700
Wire Wire Line
	1500 8600 1450 8600
NoConn ~ 1500 8500
Text Notes 1400 8200 0    50   ~ 0
Power Supply for RPi,Display\nand Router
$Comp
L power:+5V #PWR016
U 1 1 5EC55DCB
P 1450 8900
F 0 "#PWR016" H 1450 8750 50  0001 C CNN
F 1 "+5V" V 1450 9050 50  0000 L CNN
F 2 "" H 1450 8900 50  0001 C CNN
F 3 "" H 1450 8900 50  0001 C CNN
	1    1450 8900
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5EC55572
P 1450 8700
F 0 "#PWR015" H 1450 8450 50  0001 C CNN
F 1 "GND" V 1450 8550 50  0000 R CNN
F 2 "" H 1450 8700 50  0001 C CNN
F 3 "" H 1450 8700 50  0001 C CNN
	1    1450 8700
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR014
U 1 1 5EC23B7C
P 1450 8600
F 0 "#PWR014" H 1450 8450 50  0001 C CNN
F 1 "+12V" V 1450 8750 50  0000 L CNN
F 2 "" H 1450 8600 50  0001 C CNN
F 3 "" H 1450 8600 50  0001 C CNN
	1    1450 8600
	0    -1   -1   0   
$EndComp
$Comp
L custom_symbols:Pololu_D24V50F5 PS1
U 1 1 5EC1C6D0
P 1800 8650
F 0 "PS1" H 1650 9000 50  0000 L CNN
F 1 "Pololu_D24V50F5" H 1650 8900 50  0000 L CNN
F 2 "custom_footprints:Pololu_D24V50F5" H 1800 9100 50  0001 C CNN
F 3 "https://www.pololu.com/file/0J1436/d24v50f5-step-down-voltage-regulator-dimensions.pdf" H 1800 9100 50  0001 C CNN
F 4 "https://eckstein-shop.de/Pololu-5V-5A-Step-Down-Spannungsregler-D24V50F5" H 1800 8650 50  0001 C CNN "Buy_Link"
	1    1800 8650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 6500 2050 6600
Wire Wire Line
	2300 6500 2050 6500
$Comp
L power:GND #PWR03
U 1 1 5EB6FE19
P 2050 6600
F 0 "#PWR03" H 2050 6350 50  0001 C CNN
F 1 "GND" H 2050 6400 50  0000 C CNN
F 2 "" H 2050 6600 50  0001 C CNN
F 3 "" H 2050 6600 50  0001 C CNN
	1    2050 6600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J1
U 1 1 5ED4D683
P 1200 3500
F 0 "J1" H 1250 3817 50  0000 C CNN
F 1 "Buttons" H 1250 3726 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x03_P2.54mm_Vertical" H 1200 3500 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=C151%2FWSL_6G_DB.PDF" H 1200 3500 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/wannenstecker-6-polig-gerade-wsl-6g-p85732.html?&trstct=pol_0&nbc=1" H 1200 3500 50  0001 C CNN "Buy_Link"
	1    1200 3500
	1    0    0    -1  
$EndComp
Connection ~ 2950 2450
Wire Wire Line
	2950 3500 2400 3500
Wire Wire Line
	2400 3500 2400 3650
Connection ~ 2950 3500
Wire Wire Line
	2400 2450 2400 2600
Wire Wire Line
	2400 2450 2950 2450
Wire Wire Line
	2400 2900 2400 3050
Wire Wire Line
	2400 3950 2400 4100
Wire Wire Line
	2400 4950 2400 5100
$Comp
L Device:D D1
U 1 1 5ED7FFC8
P 3100 2050
F 0 "D1" H 3100 2266 50  0000 C CNN
F 1 "1N4148" H 3100 2175 50  0000 C CNN
F 2 "custom_footprints:D_SOD-123_0805_combo" H 3100 2050 50  0001 C CNN
F 3 "https://www.reichelt.de/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=A200%2FRND_1N4148W_DB-EN.pdf" H 3100 2050 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/schalt-diode-75-v-150-ma-sod-123-rnd-1n4148w-p223369.html?&nbc=1" H 3100 2050 50  0001 C CNN "Buy_Link"
	1    3100 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2050 3250 2050
Text Label 2400 2050 0    50   ~ 0
DiodsToOpto
Wire Wire Line
	1500 3400 1700 3400
Wire Wire Line
	1700 3400 1700 2450
Wire Wire Line
	1700 2450 2400 2450
Connection ~ 2400 2450
Wire Wire Line
	1500 3500 2400 3500
Connection ~ 2400 3500
Wire Wire Line
	1500 3600 1700 3600
Wire Wire Line
	1700 3600 1700 4500
Wire Wire Line
	1700 4500 2400 4500
Connection ~ 2400 4500
$Comp
L Device:D_Zener D5
U 1 1 5EDC5D01
P 3350 6000
F 0 "D5" V 3350 5800 50  0000 L CNN
F 1 "D_Zener 16V" H 3100 6150 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P10.16mm_Horizontal" H 3350 6000 50  0001 C CNN
F 3 "https://cdn-reichelt.de/documents/datenblatt/A400/ZF%23MOT.pdf" H 3350 6000 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/zenerdiode-16-v-0-5-w-do-35-zf-16-p23117.html?&trstct=pol_0&nbc=1" H 3350 6000 50  0001 C CNN "Buy_Link"
	1    3350 6000
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 5550 3350 5550
Wire Wire Line
	3350 5550 3350 5850
Connection ~ 4000 5550
Wire Wire Line
	3350 6150 3350 6400
Wire Wire Line
	2400 3050 2050 3050
Wire Wire Line
	2050 3050 2050 4100
Wire Wire Line
	2400 4100 2050 4100
Connection ~ 2050 4100
Wire Wire Line
	2050 4100 2050 5100
Wire Wire Line
	2400 5100 2050 5100
Wire Wire Line
	2050 2050 2050 3050
Wire Wire Line
	2050 2050 2950 2050
Connection ~ 2050 3050
$Comp
L power:+3.3V #PWR04
U 1 1 5EDF34B0
P 750 3300
F 0 "#PWR04" H 750 3150 50  0001 C CNN
F 1 "+3.3V" H 765 3473 50  0000 C CNN
F 2 "" H 750 3300 50  0001 C CNN
F 3 "" H 750 3300 50  0001 C CNN
	1    750  3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 3600 750  3600
Wire Wire Line
	750  3600 750  3500
Wire Wire Line
	1000 3500 750  3500
Connection ~ 750  3500
Wire Wire Line
	750  3500 750  3400
Wire Wire Line
	1000 3400 750  3400
Connection ~ 750  3400
Wire Wire Line
	750  3400 750  3300
Text Label 1500 3400 0    50   ~ 0
MainRef
Text Label 1500 3500 0    50   ~ 0
UWRef1
Text Label 1500 3600 0    50   ~ 0
UWRef2
Wire Wire Line
	2050 6300 2300 6300
Wire Wire Line
	3350 6400 3350 6600
Wire Wire Line
	2050 5100 2050 5550
Connection ~ 2050 5100
Wire Wire Line
	2050 5850 2050 6300
Text Notes 2600 1750 0    50   ~ 0
Cricuit for Buttons and horn
Wire Wire Line
	2900 6400 3350 6400
Wire Wire Line
	2900 6500 3000 6500
Wire Wire Line
	3000 6500 3000 6800
Wire Wire Line
	3000 6800 3050 6800
$Comp
L power:GND #PWR031
U 1 1 5ED02FB2
P 1650 10350
F 0 "#PWR031" H 1650 10100 50  0001 C CNN
F 1 "GND" H 1650 10200 50  0000 C CNN
F 2 "" H 1650 10350 50  0001 C CNN
F 3 "" H 1650 10350 50  0001 C CNN
	1    1650 10350
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 10150 1650 10150
Wire Wire Line
	1700 10200 1700 10150
Wire Wire Line
	1750 10200 1700 10200
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 5ECF5ED9
P 1950 10200
F 0 "J5" H 2030 10192 50  0000 L CNN
F 1 "12V" H 2030 10101 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 1950 10200 50  0001 C CNN
F 3 "https://secure.reichelt.com/index.html?ACTION=7&LA=3&OPEN=0&INDEX=0&FILENAME=C100%2FRND_800774_ENG_TDS.pdf" H 1950 10200 50  0001 C CNN
F 4 "https://secure.reichelt.com/de/de/stiftleiste-2-pol-rm-5-08-mm-0-rnd-205-00188-p170487.html?&nbc=1&trstct=lsbght_sldr::170417" H 1950 10200 50  0001 C CNN "Buy_Link"
	1    1950 10200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 10300 1700 10300
Wire Wire Line
	1700 10300 1700 10350
Wire Wire Line
	1700 10350 1650 10350
Wire Wire Line
	1500 8800 1450 8800
$Comp
L power:GND #PWR0101
U 1 1 5EB499CF
P 1450 8800
F 0 "#PWR0101" H 1450 8550 50  0001 C CNN
F 1 "GND" V 1450 8650 50  0000 R CNN
F 2 "" H 1450 8800 50  0001 C CNN
F 3 "" H 1450 8800 50  0001 C CNN
	1    1450 8800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5EC1876A
P 3700 8550
F 0 "#PWR0102" H 3700 8300 50  0001 C CNN
F 1 "GND" V 3705 8422 50  0000 R CNN
F 2 "" H 3700 8550 50  0001 C CNN
F 3 "" H 3700 8550 50  0001 C CNN
	1    3700 8550
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0103
U 1 1 5EC198C2
P 3700 8700
F 0 "#PWR0103" H 3700 8550 50  0001 C CNN
F 1 "+3.3V" V 3715 8828 50  0000 L CNN
F 2 "" H 3700 8700 50  0001 C CNN
F 3 "" H 3700 8700 50  0001 C CNN
	1    3700 8700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 1550 6250 1850
NoConn ~ 6150 1850
Wire Wire Line
	5650 4550 5850 4550
NoConn ~ 5750 4450
$Comp
L Graphic:Logo_Open_Hardware_Small Logo1
U 1 1 5ED95C89
P 5900 5650
F 0 "Logo1" H 5800 5950 50  0000 L CNN
F 1 "Symbol KiCad" H 5900 5400 50  0000 C CNN
F 2 "Symbol:KiCad-Logo2_5mm_SilkScreen" H 5900 5650 50  0001 C CNN
F 3 "~" H 5900 5650 50  0001 C CNN
	1    5900 5650
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small Logo2
U 1 1 5ED96A9A
P 6650 5650
F 0 "Logo2" H 6550 5950 50  0000 L CNN
F 1 "Symbol OSHW" H 6400 5400 50  0000 L CNN
F 2 "Symbol:OSHW-Logo_5.7x6mm_SilkScreen" H 6650 5650 50  0001 C CNN
F 3 "~" H 6650 5650 50  0001 C CNN
	1    6650 5650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
