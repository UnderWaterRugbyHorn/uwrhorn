#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import json
import copy
import datetime
import horntimers
import hornevents
import traceback

Logger = logging.getLogger(__name__)


class Game():

    def __init__(self, mqtt_publish, sound_end_sequence, update_score_gameday):
        self.mqtt_publish = mqtt_publish
        self.sound_end_sequence = sound_end_sequence
        self.update_score_gameday = update_score_gameday
        self.set_state_new()
        self.set_config_default()
        self.protocol = []
        self.linked_timers = {}
        self.saved_states = {}
        self.timeout_id = 0
        self.penaltythrow_id = 0

        self.last_published_time = ''
        self.last_published_game_time = ''

        # initialise timers
        self.game_timer = horntimers.Timer(
            self.config['game_length'],
            self.game_timer_finished)
        self.penaltythrow_timer = horntimers.Timer(
            self.config['penaltythrow_length'],
            self.penaltythrow_timer_finished)
        self.halftime_break_timer = horntimers.Timer(
            self.config['halftime_break_length'],
            self.halftime_break_timer_finished)
        self.timeout_timer = horntimers.Timer(
            self.config['timeout_length'],
            self.timeout_timer_finished)
        self.overtime_timer = horntimers.CountUpTimer()
        self.total_game_timer = horntimers.AddedTimers(self.game_timer,
                                                       self.overtime_timer)

        # add a game_stop at the very beginning
        self.add_game_stop()

    def set_state_new(self):
        self.state = {
            'is_gametime_running': False,
            'is_match_over': False,
            'is_penaltythrow': False,
            'is_timeout': False,
            'is_halftime_break': False,
            'is_extra_halftime': False,
            'n_halftime': 1,
            'has_started': False,
            'start_time': '',
            'end_time': '',
            'score_blue': 0,
            'score_white': 0,
            'actual_halftime_lengths':{}}

    def set_config_default(self):
        self.config = {
            'game_length': 900,
            'extra_game_length': 900,
            'penaltytime_length': 120,
            'penaltythrow_length': 45,
            'timeout_length': 60,
            'matchpenalty_length': 300,
            'halftime_break_length': 300,
            'n_halftime_total': 2,
            'continuous_time': False,
            'game_id': -1,
            'teamname_blue': 'Blue',
            'teamname_white': 'White'}

    def is_running(self):
        ''' Checks if the game is stopped.

        :returns: TRUE if game timer or penaltythrow timer or both are
            running, else FALSE.
        '''
        if (self.total_game_timer.is_running() or
            self.penaltythrow_timer.is_running()):
            return True
        else:
            return False

    def configure(self, check_sanity=True, **kwargs):
        ''' Sets all the configureate of this game. All times are in seconds.
        This function does not check if it is wise to change the settigs at this
        moment. It should therefore only be used internally ater such a check.

        Kwargs:
            game_length (int): Length of one halftime (Halbzeit).
            extra_game_length (int): Length of one extra halftime (Verlängerung).
            penaltytime_length (int): Length of one time penalty (Strafzeit).
            penaltythrow_length (int): Length of one penalty throw (Strafwurf).
            timeout_length (int): Lenght of timeout (Auszeit).
            matchpenalty_length (int): Lenght of power play time after a player
                was given a match penalty (Hinausstellung).
            halftime_break_length (int): Break between halftimes (Halbzeitpause).
            n_halftime_total (int): Number of halftimes per game.
            continuous_time (bool): Don't stop time for any penalties or goals.
        '''
        if check_sanity:
            if self.is_running():
                msg = ('This game is running. You can only reconfigure '
                       'it, if it is stopped.')
                self.mqtt_publish('command/answer', msg)
                return
            if not set(kwargs.keys()) <= set(self.config.keys()):
                msg = ('One of the following keys did not match any known'
                       'configuration keys. ({})'.format(kwargs.keys()))
                self.mqtt_publish('command/answer', msg, is_error=True)
                return
        for key in kwargs:
            if kwargs[key] is None:
                # skip new configureations, which are None.
                continue
            if key == "continuous_time":
                self.config[key] = (str(kwargs[key])[0].lower() == 't')
            elif 'teamname' in key:
                self.config[key] = kwargs[key]
            else:
                n = int(kwargs[key])
                if key == 'game_id':
                    not_smaller = -1
                else:
                    not_smaller = 1
                if check_sanity and n < not_smaller:
                    msg = ('The setting {} needs to be larger or equal to {}, given {}.'
                           ''.format(key, not_smaller, kwargs[key]))
                    self.mqtt_publish('command/answer', msg, is_error=True)
                else:
                    self.config[key] = n

        # set timer to new time length
        self.game_timer.time_length = self.config['game_length']
        self.game_timer.continuous_time = self.config['continuous_time']
        self.penaltythrow_timer.time_length = self.config['penaltythrow_length']
        self.halftime_break_timer.time_length = self.config['halftime_break_length']
        self.timeout_timer.time_length = self.config['timeout_length']
        self.publish_config()

    def reset_game(self,
                   check_sanity=True,
                   start_new_game=False,
                   new_game_id=-1,
                   teamname_blue=None,
                   teamname_white=None):
        ''' This function delets all the data of the current game, resets all
        timers, so that a new game can start from the beginning. This function
        does not check if it is wise to do so. It should therefore only be
        called after such a check was done.
        '''
        if teamname_blue is None:
            teamname_blue = 'Blue'
        if teamname_white is None:
            teamname_white = 'White'
        Logger.info('reset game')
        if check_sanity:
            if (self.is_running() and (not self.config['continuous_time'])):
                msg = 'Stop the game before you reset it.'
                self.mqtt_publish('command/answer', msg)
                return
        if start_new_game:
            self.config['game_id'] = int(new_game_id)
            self.config['teamname_blue'] = teamname_blue
            self.config['teamname_white'] = teamname_white

        self.set_state_new()
        self.protocol = []
        self.timeout_id = 0
        self.penaltythrow_id = 0
        self.linked_timers = {}

        self.game_timer.reset()
        self.game_timer.time_length = self.config['game_length']
        self.penaltythrow_timer.reset()
        self.halftime_break_timer.reset()
        self.timeout_timer.reset()
        self.overtime_timer.reset()
        self.update_score_gameday()

        # add game stop at the very beginning
        self.add_game_stop()

        self.publish_config()
        self.publish_state()
        self.publish_protocol()
        self.publish_time(ignore_last_published_game_time=True)

    def select_game(self, new_id, teamname_blue=None, teamname_white=None):
        ''' Sets the game Id of the current game. The game Id can only be set,
        if it has not been set before or if the game has not started, jet.
        This was supposed to stop confusion during the saving of the score.

        :param new_id: New game Id
        :type new_id: int or string
        '''
        new_id = int(new_id)
        if self.is_running():
            msg = ('You can only choose a differnt game, if the current game '
                   'is not running. The current game has the id {}.'
                   ''.format(self.config['game_id']))
            self.mqtt_publish('command/answer', msg, is_error=True)
            return
        if new_id == self.config['game_id']:
            # the wanted game id is already set. only update the teamnames.
            self.configure(teamname_blue=teamname_blue,
                           teamname_white=teamname_white)
            return
        Logger.info('set to new game with id {}, the old id was {}'
                    ''.format(new_id, self.config['game_id']))
        # save the curent game before changing it
        self.saved_states[str(self.config['game_id'])] = copy.deepcopy(self.get_state())
        Logger.info('saved states {}'.format(self.saved_states))
        if str(new_id) in self.saved_states:
            # The new game has an old saved sate. Use this one.
            self.set_state(self.saved_states[str(new_id)])
            # if the teamnames changed in the meantime, update them
            self.configure(teamname_blue=teamname_blue,
                           teamname_white=teamname_white)
        elif self.config['game_id'] == -1:
            # A game not connected to the gameday table has already started.
            # Use this game with the given new game_id.
            self.configure(game_id=new_id,
                           teamname_blue=teamname_blue,
                           teamname_white=teamname_white)
        else:
            self.reset_game(start_new_game=True,
                            new_game_id=new_id,
                            teamname_blue=teamname_blue,
                            teamname_white=teamname_white)
        self.update_score_gameday()
        self.publish_all()

    def game_timer_finished(self):
        ''' This function is called when the game_timer has finished. It
        checks wether a penalty ball is running to prolong the game time and if
        not stops the game (calls :meth:`stop_game`).
        '''
        Logger.info('game timer has finished')
        try:
            if self.state['is_penaltythrow']:
                msg = ('Game time will be prolonged till game is over.')
                self.mqtt_publish('command/answer', msg)
                self.overtime_timer.start()
            else:
                self.stop_game()
        except Exception as e:
            msg = str(e) + "\n\n" + traceback.format_exc()
            self.mqtt_publish("command/answer", msg, is_error=True)

    def penaltythrow_timer_finished(self):
        ''' This function is called, when the penaltythrow_timer has finished.
        It will stop the game (calls :meth:`stop_game`).'''
        Logger.info('penaltythrow timer has ended')
        try:
            self.stop_game()
        except Exception as e:
            msg = str(e) + "\n\n" + traceback.format_exc()
            self.mqtt_publish("command/answer", msg, is_error=True)

    def timeout_timer_finished(self):
        ''' This function is called, when the timeout_timer has finished or
        by force starting the game. A normal start (button press) by the main
        referee will not cause the game to start during timeout.
        '''
        Logger.info('timeout timer has finished')
        try:
            self.timeout_timer.reset()
            self.state['is_timeout'] = False
            self.publish_state()
        except Exception as e:
            msg = str(e) + "\n\n" + traceback.format_exc()
            self.mqtt_publish("command/answer", msg, is_error=True)

    def halftime_break_timer_finished(self):
        ''' This function is called when the halftime_break_timer has finished
        or by force starting the game. A normal start (button press) by the
        main referee will not cause the game to start during the
        halftime break.
        '''
        Logger.info('halftime break timer has finished')
        try:
            self.state['is_halftime_break'] = False
            self.state['actual_halftime_lengths'][self.state['n_halftime']] = self.total_game_timer.get_time()
            for timer in self.linked_timers.values():
                timer.prepare_for_next_halftime()
            self.game_timer.reset()
            self.overtime_timer.reset()
            self.halftime_break_timer.reset()
            self.state['n_halftime'] += 1
            self.publish_state()
        except Exception as e:
            msg = str(e) + "\n\n" + traceback.format_exc()
            self.mqtt_publish("command/answer", msg, is_error=True)

    def start_game(self):
        ''' This function is called when the main referee presses his
        button once, to start the game or a penalty ball. If he
        presses the button multile times, when the game is stopped, the
        game will be started and stopped again at the second button press.
        During timeout or the halftime break the game will not be started.
        Use :meth:`force_start_game` to start the game during these times.
        '''
        Logger.info('start game')
        if not self.state['has_started']:
            self.state['has_started'] = True
            self.update_score_gameday()
            self.state['start_time'] = datetime.datetime.now(datetime.timezone.utc).isoformat(timespec="seconds")
        if self.state['is_penaltythrow']:
            if self.state['is_timeout']:
                Logger.info('cannot start a penaltythrow,because it '
                            'is timeout')
            else:
                Logger.info('start penaltythrow')
                self.penaltythrow_timer.start()
        if (self.state['is_halftime_break'] or self.state['is_timeout']):
            Logger.info('cannot start game, because its halftime '
                        'break or timeout')
        else:
            self.game_timer.start()
        self.state['is_gametime_running'] = self.is_running()
        self.publish_state()

    def force_start_game(self):
        ''' This will end a timeout or a halftime break and start the game
        (calls :meth:`start_game`).
        '''
        Logger.info('force_start_game')
        if self.state['is_timeout']:
            self.timeout_timer_finished()
        if self.state['is_halftime_break']:
            self.halftime_break_timer_finished()
        self.start_game()
        self.publish_state()

    def stop_game(self, force=False):
        ''' This will stop the game, unless if in continuous time mode.

        .. todo:: Currently, if the uw referee stops a penalty throw during
            overtime, the horn will sound multiple times. Is this good?
        '''
        Logger.info('stop game')
        self.game_timer.stop(force)
        self.overtime_timer.stop()
        self.penaltythrow_timer.stop()
        prepare_end_sequence = False
        if self.state['is_gametime_running']:
            # If the game was previously running add a game_stop
            self.add_game_stop()
        if self.state['is_penaltythrow'] and self.state['is_gametime_running']:
            if (self.game_timer.has_finished() and
                not self.state['is_match_over']):
                # the game was extended to finish
                # this penalty ball, set halftime over
                self.halftime_over()
                prepare_end_sequence = True
            elif self.penaltythrow_timer.has_finished():
                # if the penalty timer has run out, sound a sequence
                prepare_end_sequence = True
            self.set_penaltythrow(False)
        elif not self.state['is_halftime_break']:
            if self.game_timer.has_finished():
                if not self.state['is_match_over']:
                    # In this case we are at the end of a halftime, so the horn sequence needs to sound.
                    prepare_end_sequence = not self.state['is_halftime_break']
                    self.halftime_over()
        self.state['is_gametime_running'] = self.is_running()
        self.publish_state()
        if prepare_end_sequence:
            self.sound_end_sequence()

    def halftime_over(self):
        ''' This function is called at the end of each halftime. Ether when
        the game time is up or when the last penalty throw is over. Depending
        on what takes longer.
        '''
        Logger.info('halftime over')

        if self.state['n_halftime'] < self.config['n_halftime_total']:
            self.state['is_halftime_break'] = True
            self.halftime_break_timer.restart()
            self.protocol[-1].add_event(event_type='halftimebreak',
                                        timer_id=self.state['n_halftime'],
                                        desc='Halftime Break')
            self.publish_protocol()
        else:
            self.state['is_match_over'] = True
            self.state['is_extra_halftime'] = False
            self.state['end_time'] = datetime.datetime.now(datetime.timezone.utc).isoformat(timespec="seconds")
        self.publish_state()
        # Moved sounding of horn to the end of the stop_game function to allow the game state to be published before the horn 
        # sequence. This was necessary, because the sound_horn_sequence only finishes after the horn sequence is done
        # due to the new broadcasting of horn events via mqtt. We want halftime timer to start immediately and the game state to update.

    def set_penaltythrow(self, turn_on):
        ''' Toggles the penatly ball state of this class. If
        :meth:`is_running` is True, this method will do nothing.

        :param bool turn_on: TRUE to turn on, FALSE to turn off
        '''
        turn_on = (str(turn_on)[0].lower() == 't')
        Logger.info('toggle pentaly throw ' + str(turn_on))
        if (self.is_running() and
            (not self.config['continuous_time'])):
            msg = 'Stop the game before you set the penaltythrow mode.'
            self.mqtt_publish('command/answer', msg)
            return None
        else:
            self.penaltythrow_timer.reset()
            if turn_on:
                if self.state['is_halftime_break']:
                    self.halftime_break_timer.reset()
                self.state['is_penaltythrow'] = True
                self.penaltythrow_id += 1
                self.publish_state()
                return self.penaltythrow_id
            else:
                self.state['is_penaltythrow'] = False
                self.publish_state()
                return None

    def set_extra_halftime(self, turn_on):
        ''' Toggles the extra halftime state of this class.

        If turn_on = True:
           Addes an extra halftime to the game, in case a winner is needed
           and it is still a draw after the match.

        :param bool turn_on: TRUE to turn on, FALSE to turn off
        '''
        turn_on = (str(turn_on)[0].lower() == 't')
        Logger.info('toggle extra halftime ' + str(turn_on))
        if self.is_running():
            msg = 'You cannot add or remove an extra halftime, while the game is running.'
            self.mqtt_publish('command/answer', msg)
        elif turn_on:
            if not self.state['is_match_over']:
                msg = 'The match need to end first, before an extra halftime can be added.'
                self.mqtt_publish('command/answer', msg)
            else:
                self.state['is_match_over'] = False
                self.state['is_extra_halftime'] = True
                self.game_timer.time_length = (
                    self.config['extra_game_length'])
                self.state['is_halftime_break'] = True
                self.halftime_break_timer.restart()
                self.protocol[-1].add_event(event_type='halftimebreak',
                                            timer_id=self.state['n_halftime'],
                                            desc='Halftime Break')
        elif self.state['is_extra_halftime']:
            if not self.state['is_halftime_break']:
                self.state['n_halftime'] -= 1
            self.state['is_match_over'] = True
            self.state['is_extra_halftime'] = False
            self.game_timer.time_length = (
                self.config['game_length'])
            self.state['is_halftime_break'] = False
            self.halftime_break_timer.reset()
            self.game_timer.set_finished()
            for event in self.protocol[-1].events:
                if event.event_type == 'halftimebreak':
                    self.protocol[-1].delete_event(event.id)
        self.publish_protocol()
        self.publish_state()

    def get_state(self, game_id=None):
        ''' Creates a dictionary with all important information of the currnent game.
        This dictionary can be used to setup this game again.

        :returns: the state of this class
        :rtype: dict
        '''
        if game_id is None or game_id == self.config['game_id']:
            state = {'state': self.state,
                     'config': self.config,
                     'game_timer': self.game_timer.get_state(),
                     'penaltythrow_timer': self.penaltythrow_timer.get_state(),
                     'halftime_break_timer': self.halftime_break_timer.get_state(),
                     'timeout_timer': self.timeout_timer.get_state(),
                     'overtime_timer': self.overtime_timer.get_state(),
                     'protocol': [gs.get_state() for gs in self.protocol],
                     'linked_timers': [lt.get_state() for lt in self.linked_timers.values()]}
            return state
        if str(game_id) in self.saved_states:
            return self.saved_states[str(game_id)]
        return None

    def set_state(self, new_state):
        ''' Sets this class' game state. This function needs to be treated with care,
        since no sanity check is performed before applying the new state.

        :param dict new_state: New state of this class.
        '''
        if sorted(self.get_state().keys()) != sorted(new_state.keys()):
            raise ValueError('this new state has not the correct keys')
        if sorted(self.state.keys()) != sorted(new_state['state'].keys()):
            raise ValueError('the value of "state" has not the correct keys')
        if sorted(self.config.keys()) != sorted(new_state['config'].keys()):
            raise ValueError('the value of "configuration" has not the correct keys')
        Logger.info('set new state = ' + str(new_state))
        self.state.update(**new_state['state'])
        self.configure(**new_state['config'])
        self.game_timer.set_state(new_state['game_timer'])
        self.penaltythrow_timer.set_state(new_state['penaltythrow_timer'])
        self.halftime_break_timer.set_state(new_state['halftime_break_timer'])
        self.timeout_timer.set_state(new_state['timeout_timer'])
        self.overtime_timer.set_state(new_state['overtime_timer'])
        self.linked_timers = {}
        for lt in new_state['linked_timers']:
            new_lt = horntimers.LinkedTimer(total_game_timer=self.total_game_timer, **lt)
            self.linked_timers[new_lt.id] = new_lt
        self.protocol = []
        for gs_state in new_state['protocol']:
            new_gs = hornevents.GameStop(None, self, None, None, None)
            new_gs.set_state(gs_state)
            self.protocol.append(new_gs)

    def add_game_stop(self):
        new_id = len(self.protocol)
        game_stop = hornevents.GameStop(new_id,
                                        self,
                                        self.state['n_halftime'],
                                        self.total_game_timer.get_time(),
                                        self.total_game_timer.get_countdown_time_string())
        self.protocol.append(game_stop)
        self.publish_protocol()

    def add_event(self, game_stop_id, event_type, teamcolor='', **kwargs):
        game_stop_id = int(game_stop_id)
        if not self.add_event_check_single(game_stop_id, event_type):
            return
        kwargs = self.add_event_adjust_state(game_stop_id, event_type, teamcolor, **kwargs)
        if not kwargs:
            return
        self.protocol[game_stop_id].add_event(**kwargs)
        self.publish_state()
        self.publish_protocol()

    def add_event_check_single(self, game_stop_id, event_type):
        if event_type in ['goal', 'penaltythrow', 'halftimebreak']:
            if self.protocol[game_stop_id].has_event_of_type(event_type):
                msg = ('An event of type {} can only be addd once per game stop.'
                       ''.format(event_type))
                self.mqtt_publish('command/answer', msg)
                return False
        return True

    def add_event_adjust_state(self, game_stop_id, event_type,
                               teamcolor='', is_edit=False, **kwargs):
        '''This part off add_event can also be used toghether with edit_event.
        '''
        Logger.debug('add new event {}'.format(event_type))
        kwargs['event_type'] = event_type
        kwargs['teamcolor'] = teamcolor
        kwargs['time_length'] = None
        if event_type == "penaltytime":
            kwargs['time_length'] = self.config['penaltytime_length']
        elif event_type == "doublepenaltytime":
            kwargs['time_length'] = 2 * self.config['penaltytime_length']
        elif event_type == "matchpenalty":
            kwargs['time_length'] = self.config['matchpenalty_length']
        elif event_type == "goal":
            if teamcolor == 'blue':
                self.state['score_blue'] += 1
            elif teamcolor == 'white':
                self.state['score_white'] += 1
            else:
                raise ValueError('The teamcolor "{}" is not known. Use "blue" or "white".'
                                 ''.format(teamcolor))
            diff_blue = 0
            diff_white = 0
            for gs in self.protocol[game_stop_id+1:]:
                diff = gs.adjust_score(teamcolor, 1)
                diff_blue += diff[0]
                diff_white += diff[1]
            self.update_score_gameday()
            self.publish_state()
            if is_edit:
                if teamcolor == 'blue':
                    diff_white += 1
                elif teamcolor == 'white':
                    diff_blue += 1
            kwargs['extra'] = "{} : {}".format(self.state['score_blue']-diff_blue,
                                               self.state['score_white']-diff_white)
        elif event_type == "timeout":
            if (game_stop_id != len(self.protocol)-1 or
                (self.is_running() and not self.config['continuous_time'])):
                msg = ('WARNING: You added a timeout not to the last game stop '
                       'or while the game was running. '
                       'The timeout is added to the protocol, but the timeout '
                       'mode is not toggled on.')
                self.mqtt_publish('command/answer', msg)
            else:
                if self.state['is_halftime_break']:
                    msg = 'Timeout is not possible during the halftime break.'
                    self.mqtt_publish('command/answer', msg)
                    return False
                if self.state['is_timeout']:
                    msg = ('A timeout is already running. Please wait until it is '
                           'over to start a new one.')
                    self.mqtt_publish('command/answer', msg)
                    return False
                self.timeout_id += 1
                kwargs['timer_id'] = self.timeout_id
                self.timeout_timer.restart()
                if self.config['continuous_time']:
                    self.game_timer.stop(force=True)
                    self.state['is_gametime_running'] = self.is_running()
                    self.publish_state()
                self.state['is_timeout'] = True
        elif event_type == "penaltythrow":
            if (game_stop_id != len(self.protocol)-1 or
                (self.is_running() and not self.config['continuous_time'])):
                msg = ('WARNING: You added a penalty throw not to the last game stop '
                       'or while the game was running. '
                       'The penalty is added to the protocol, but the penalty throw mode is '
                       'not toggled on.')
                self.mqtt_publish('command/answer', msg)
            else:
                if self.state['is_penaltythrow']:
                    msg = ('The penatly throw mode is already toggled on. You '
                           'can only add one penalty throw to the latest game stop.')
                    self.mqtt_publish('command/answer', msg)
                    return False
                if self.state['is_halftime_break']:
                    self.halftime_break_timer.reset()
                self.penaltythrow_timer.reset()
                self.state['is_penaltythrow'] = True
                self.penaltythrow_id += 1
                kwargs['timer_id'] = self.penaltythrow_id
        return kwargs

    def delete_event(self, game_stop_id, event_id):
        game_stop_id = int(game_stop_id)
        event_id = int(event_id)
        event = self.protocol[game_stop_id].get_event(event_id)
        if event is None:
            msg = ('could not find an event with the id {} at the gamestop with the id {}.'
                   ''.format(event_id, game_stop_id))
            self.mqtt_publish('command/answer', msg)
            return
        self.delete_event_adjust_state(game_stop_id, event)
        self.protocol[game_stop_id].delete_event(event_id)
        self.publish_state()
        self.publish_protocol()
        self.mqtt_publish('state/game_time', '', qos=0, retain=False)
        self.publish_time(ignore_last_published_game_time=True)
        return

    def delete_event_adjust_state(self, game_stop_id, event):
        ''' This part of_event handles the general stuff for the whole game
        only. It is separeted, so it can be used together with edit_event.
        '''
        if event.event_type == 'goal':
            if event.teamcolor == 'blue':
                self.state['score_blue'] -= 1
            elif event.teamcolor == 'white':
                self.state['score_white'] -= 1
            else:
                raise ValueError('The teamcolor "{}" is not known. Use "blue" or "white".'
                             ''.format(event.teamcolor))
            for gs in self.protocol[game_stop_id:]:
                gs.adjust_score(event.teamcolor, -1)
            self.update_score_gameday()
            self.publish_state()
        elif event.event_type == 'timeout':
            if (self.state['is_timeout']): # and game_stop_id == len(self.protocol)-1):
                self.timeout_timer.reset()
                self.state['is_timeout'] = False
        elif event.event_type == 'penaltythrow':
            if (self.is_running() and
                not self.config['continuous_time'] and
                game_stop_id == len(self.protocol)-1):
                msg = 'Stop the game before you delete the latest penalty throw.'
                self.mqtt_publish('command/answer', msg)
                return
            if (self.state['is_penaltythrow'] and
                game_stop_id == len(self.protocol)-1):
                self.penaltythrow_timer.reset()
                self.state['is_penaltythrow'] = False

    def edit_event(self, game_stop_id, event_id, event_type, teamcolor='', **kwargs):
        game_stop_id = int(game_stop_id)
        event_id = int(event_id)
        event = self.protocol[game_stop_id].get_event(event_id)
        if event is None:
            msg = ('Could not find an event with the id {} at the gamestop with the id {}.'
                   ''.format(event_id, game_stop_id))
            self.mqtt_publish('command/answer', msg)
            return
        if not event_type == event.event_type:
            if not self.add_event_check_single(game_stop_id, event_type):
                return
            kwargs = self.add_event_adjust_state(game_stop_id, event_type, teamcolor, **kwargs)
            if not kwargs:
                # the new event data is not correct
                return
            self.delete_event_adjust_state(game_stop_id, event)
        elif event_type == 'goal' and not teamcolor == event.teamcolor:
            kwargs = self.add_event_adjust_state(game_stop_id, event_type, teamcolor,
                                                 is_edit=True, **kwargs)
            if not kwargs:
                # the new event data is not correct
                return
            self.delete_event_adjust_state(game_stop_id, event)
        else:
            kwargs['teamcolor'] = teamcolor
        self.protocol[game_stop_id].edit_event(event_id, **kwargs)
        self.publish_state()
        self.publish_protocol()
        return

    def add_linked_timer(self, time_length, start_time, teamcolor='', halftime=None):
        if not self.linked_timers:
            new_id = 0
        else:
            new_id = max(self.linked_timers.keys()) + 1
        # this part is only nessecary, when the penalty timer started in a
        # previos halftime, ie. when the halfetime of the time is samller then
        # the current halftime. The time lengths of these halftimes need to be
        # subtracted from the start time of the timer.
        if halftime is not None:
            while halftime < self.state['n_halftime']:
                if halftime in self.state['actual_halftime_lengths']:
                    start_time = start_time - self.state['actual_halftime_lengths'][halftime]
                halftime += 1
        linked_timer = horntimers.LinkedTimer(new_id, time_length, start_time,
                                              self.total_game_timer,
                                              teamcolor=teamcolor)
        self.linked_timers[new_id] = linked_timer
        self.publish_time(ignore_last_published_game_time=True)
        return new_id

    def delete_linked_timer(self, timer_id):
        if timer_id in self.linked_timers:
            del self.linked_timers[timer_id]
            self.publish_time(ignore_last_published_game_time=True)

    def set_linkedtimer_active(self, timer_id, active):
        timer_id = int(timer_id)
        if timer_id in self.linked_timers:
            self.linked_timers[timer_id].active = active
            self.publish_time(ignore_last_published_game_time=True)
        else:
            msg = ('The linked timer index {} is out of range.'
                   ''.format(timer_id))
            self.mqtt_publish('command/answer', msg)

    def publish_all(self):
        self.publish_config()
        self.publish_state()
        self.publish_protocol()

    def publish_config(self):
        ''' publish configuration
        '''
        self.mqtt_publish('state/game_config', self.config, retain=True)

    def publish_state(self):
        ''' publish configuration
        '''
        self.mqtt_publish('state/game_state', self.state, retain=True)

    def publish_time(self, ignore_last_published_game_time=False):
        ''' Publish the currently most important timer as well as the
        current real time via mqtt. The message will only be published, if it
        is differnt from the previos message.

        The order for the most important timer is as follows:
            * timeout timer
            * penalty timer
            * halftime break timer
            * game timer
        '''
        datetime_now = datetime.datetime.now(datetime.timezone.utc)
        if self.last_published_time != datetime_now.second:
            self.last_published_time = datetime_now.second
            self.mqtt_publish('state/real_time', datetime_now.isoformat(timespec="seconds"),
                              qos=0)

        time = {'game': self.total_game_timer.get_countdown_time_string(),
                'timeout': {'id': self.timeout_id,
                            'countdown': self.timeout_timer.get_countdown_time_string()},
                'halftimebreak': {'id': self.state['n_halftime'],
                                  'countdown': self.halftime_break_timer.get_countdown_time_string()},
                'penaltythrow': {'id': self.penaltythrow_id,
                                 'countdown': self.penaltythrow_timer.get_countdown_time_string()}}
        new_published_game_time = json.dumps(time)
        if (ignore_last_published_game_time or
            self.last_published_game_time != new_published_game_time):
            self.last_published_game_time = new_published_game_time
            time['linked_timers'] = self.get_running_linked_timers()
            self.mqtt_publish('state/game_time', time,
                              qos=0,
                              retain=True)

    def get_running_linked_timers(self):
        running_linked_timers = {}
        for lt_id, linked_timer in self.linked_timers.items():
            if not linked_timer.has_finished():
                running_linked_timers[lt_id] = {
                    "countdown": linked_timer.get_countdown_time_string(),
                    "teamcolor": linked_timer.teamcolor,
                    "percentage": linked_timer.get_percentage_remainig(),
                    "active": linked_timer.active}
        return running_linked_timers

    def publish_protocol(self):
        protocol = [gs.get_publish() for gs in self.protocol]
        self.mqtt_publish('state/protocol', protocol, retain=True)
        self.publish_time(ignore_last_published_game_time=True)
