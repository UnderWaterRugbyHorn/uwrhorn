import json

tournamentFile = open("../docker/uwrhorn_gamecontroller/data/tournament.json")
if tournamentFile is None:
    print("Could not find tournament.json file.")
    exit()
    
tournamentString = tournamentFile.readline()

tournament = json.loads(tournamentString)
roster = tournament["roster"]
gameday = tournament["gameday"]
players = {}

for id, game in tournament["game_states"].items():
    teamblue = None
    teamwhite = None
    for gameday_game in gameday:
        if int(id) == gameday_game["id"]:
            teamblue = gameday_game["blue"]
            teamwhite = gameday_game["white"]

    for timestamp in game["protocol"]:
        for event in timestamp["events"]:
            if event["event_type"] == "goal":
                nr = event["player_nr"]
                if nr == '':
                    print("Found a goal in game " + id + " where no player was selected. Continuing.")
                    continue
                team = None
                if event["teamcolor"] == "blue":
                    team = teamblue
                elif event["teamcolor"] == "white":
                    team = teamwhite
                playername = "Player " + str(nr)
                if nr in roster[team]["players"]:
                    playername = roster[team]["players"][nr]["name"]

                if playername not in players.keys():
                    players[playername] = {"goals": 1, "team": roster[team]["name"]}
                else:
                    players[playername]["goals"] += 1
                

sortedDict = dict(sorted(players.items(), key=lambda x:x[1]["goals"], reverse=True))
print("{:<6} {:<30} {:<30} {:<6}".format("Place", "Name", "Team", "Goals"))
prevGoals = 0
shift = 0
wasEqual = False
for i, (name, data) in enumerate(sortedDict.items()):
    if str(data["goals"]) == prevGoals:
        shift += 1
        wasEqual = True
    elif wasEqual:
        wasEqual = False
        shift = 0
    print("{:<6} {:<30} {:<30} {:<6}".format(str(i+1-shift) + ".", name, data["team"], str(data["goals"])))
    
    prevGoals = str(data["goals"])

